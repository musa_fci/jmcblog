
require('./bootstrap');

window.Vue = require('vue');


import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)



import Blogheader from './components/Blogheader.vue';
Vue.component('Blogheader', Blogheader);

import Blogfooter from './components/Blogfooter.vue';
Vue.component('Blogfooter', Blogfooter);

import Bloghome from './components/Bloghome.vue';
Vue.component('Bloghome', Bloghome);

import Blogadd from './components/Blogadd.vue';
Vue.component('Blogadd', Blogadd);

import Blogdetails from './components/Blogdetails.vue';
Vue.component('Blogdetails', Blogdetails);

import Bloglist from './components/Bloglist.vue';
Vue.component('Bloglist', Bloglist);

import Userregister from './components/Userregister.vue';
Vue.component('Userregister', Userregister);

import Userlogin from './components/Userlogin.vue';
Vue.component('Userlogin', Userlogin);

import Blogupdate from './components/Blogupdate.vue';
Vue.component('Blogupdate', Blogupdate);


const routes = [
    { path: '/', component: Bloghome },
    { path: '/add', component: Blogadd },
    { path: '/details', component: Blogdetails },
    { path: '/bloglist', component: Bloglist },
    { path: '/registration', component: Userregister },
    { path: '/login', component: Userlogin }
  ]


const router = new VueRouter({
    routes
    // mode: 'history'
  })


const app = new Vue({
    el: '#app',
    router
    
});
