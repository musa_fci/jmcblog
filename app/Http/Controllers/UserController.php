<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use DB;
use Session;

class UserController extends Controller
{
    public function userRegistration(UserRequest $request) {
        $reg = new User;

        $reg->name      = $request->name;
        $reg->email     = $request->email;
        $reg->password  = Hash::make($request->password);

        $reg->save();
        return response()->json([
            'successMsg' => 'User Registration Successfully...!'
        ]);
    }


    public function userLogin(Request $request) {

        $request->validate([
    		'email'		=>	'required',
    		'password'	=>	'required',	
    	]);
        
        $data = DB::table('users')->where('id',$request->id)->get(); 
        
        // dd($data);
        
        if($data->email==$request->email AND Hash::check($request->password,$data->password)){
    		Session::put('user',(array)$data);                                            
            return "Login Successfull !";
    	}else{            
            return "Login Fail !";
    	}

    }
}
