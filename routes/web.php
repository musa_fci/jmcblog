<?php


Route::get('/', function () {
    return view('welcome');
});

Route::resource('blog','BlogController');
Route::post('getBlog','BlogController@getBlog');

Route::resource('comment','CommentController');
Route::post('getComment/{id}','CommentController@getComment');

Route::post('registration','UserController@userRegistration');
Route::post('login','UserController@userLogin');